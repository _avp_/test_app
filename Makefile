SRCDIR = ./src/
LIBDIR = ./lib/

LIBSHARED = libtestsrv.so
CC = g++
CFLAGS += -std=c++11 -fPIC
OBJECTS := $(patsubst %.cpp,%.o,$(wildcard $(SRCDIR)*.cpp))

%.o: %.cpp
	@echo [CC] [CFLAGS] $<
	$(CC) -c $(CFLAGS) $< -o $@

all: $(LIBSHARED)

$(LIBSHARED) : $(OBJECTS)
	@echo [LD] $(LIBSHARED)
	$(CC) -o $(LIBDIR)$(LIBSHARED) $(OBJECTS) -shared

clean:
	@echo [clean] LIBSHARED
	@rm -f $(LIBDIR)$(LIBSHARED)
	@echo [clean] OBJECTS 
	@rm -f $(OBJECTS)	
