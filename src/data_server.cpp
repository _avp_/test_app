/*
 * data_server.cpp
 *
 *  Created on: Sep 12, 2016
 *      Author: petrov
 */

#include "data_server.h"

// access() function
#include <unistd.h>

// hash function
#include <functional>

// DataBlob Class
// ==========================================================================================

DataBlob::DataBlob(uint64_t blockSize, uint32_t blobSize)
{
	_blockSize 	= blockSize;
	_blobSize 	= blobSize;
	_state		= states::STATE_NOT_OPENED;
}

DataBlob::~DataBlob()
{
	if (_fileBlob.is_open()) {
		_fileBlob.close();
	}
}

int DataBlob::open(string fileName)
{
	int result = 0;

	if (_fileBlob.is_open()) {
		_fileBlob.close();
	}

	// проверяем существует ли файл
	if (!access( fileName.c_str(), F_OK) == 0) {
		// файла не существует
		_fileBlob.open(fileName.c_str(), fstream::out);
		_state = states::STATE_RW;
	}  else {
		// открываем
		_fileBlob.open(fileName.c_str(), fstream::in);
		_state = states::STATE_RO;
	}
	if (_fileBlob.fail()) {
		result = errs::ERR_BLOB_NOT_OPENED;
	}

	_fileName = fileName;

	return result;
}

int DataBlob::putBlock(const char* blockData)
{
	int result = 0;

	if (!_fileBlob.is_open()) {
		result = errs::ERR_BLOB_NOT_OPENED;
		goto exit;
	}

	if (_state == states::STATE_RW ) {
		if (static_cast<uint64_t>(_fileBlob.tellp()) < _blobSize * _blockSize) {
			_fileBlob.write(blockData, _blockSize);
			_fileBlob.flush();
		} else {
			result = errs::ERR_BLOB_IS_FULL;
		}
	} else {
		result = errs::ERR_BLOB_IS_RO;
	}

exit:
	return result;
}

int DataBlob::getBlock(uint64_t blockPos, char* blockData)
{
	int result = 0;

	if (!_fileBlob.is_open()) {
		result = errs::ERR_BLOB_NOT_OPENED;
		goto exit;
	}

	_fileBlob.seekp(blockPos * _blockSize, fstream::beg);
	_fileBlob.read(blockData, _blockSize);

exit:
	return result;
}

int DataBlob::getPosition()
{
	int result;

	if (!_fileBlob.is_open()) {
		result = errs::ERR_BLOB_NOT_OPENED;
		goto exit;
	}

	result = static_cast<uint64_t>(_fileBlob.tellp()) / _blockSize;

	exit:
		return result;
}

string DataBlob::getFilename()
{
	return _fileName;
}
// ==========================================================================================

// DataServer Class
// ==========================================================================================

DataServer::DataServer()
{
	_blobSize 	= 0;
	_blockSize 	= 0;

	_rBlob = 0;
	_wBlob = 0;

	_wBlobCount = 0;

	_wBlob = NULL;
	_rBlob = NULL;
}

DataServer::~DataServer()
{
	if (_wBlob != NULL) {
		delete _wBlob;
	}

	if (_rBlob != NULL) {
			delete _rBlob;
		}
}

int DataServer::init(uint64_t blockSize, uint32_t blobSize)
{
	int result = 0;

	if ((_blockSize != 0) || (_blobSize != 0)) {
		// уже проинициализирован
		result = errs::ERR_ALREADY_INITIALIZED;
	} else {
		_blockSize 	= blockSize;
		_blobSize 	= blobSize;

		_rBlob = new DataBlob(_blockSize, _blobSize);
		_wBlob = new DataBlob(_blockSize, _blobSize);

		// открываем файл в дефолтной директории
		string blobPath = string(STORAGE_DIRECTORY) + "/" + string(STORAGE_FILENAME) + "_" + to_string(_wBlobCount);
		_wBlob->open(blobPath);
	}

	return result;
}

int DataServer::putBlock(uint64_t blockId, const char* blockData)
{
	int result = 0;

	string dataString = string(blockData, _blockSize);
	size_t dataHash = hash<string>()(dataString);

	struct blockDesc bDesc;
	string blobPath;

	map<uint64_t, blockDesc>::iterator findIndexRes;
	map<size_t, uint64_t>::iterator findHashRes;

	uint32_t writePosition;

	// проверяем присутствие blockId в таблице
	findIndexRes = _tableIndex.find(blockId);
	if (findIndexRes != _tableIndex.end()) {
		result = errs::ERR_BLOCK_ID_EXIST;
		goto exit;
	}

	// проверяем присутствие хэш-функции blockData в таблице
	findHashRes = _tableHash.find(static_cast<uint64_t>(dataHash));
	if (findHashRes != _tableHash.end()) {
		// такой блок уже есть
		result = errs::ERR_BLOCK_DATA_EXIST;
		// находим первый индекс для такого блока
		findIndexRes = _tableIndex.find(findHashRes->second);
		// и добавляем описание в таблицу индексов
		_tableIndex.insert(pair<uint64_t, blockDesc>(blockId, findIndexRes->second));
		goto exit;
	}

	// добавляем блок в блоб
	writePosition = _wBlob->getPosition();
	result = _wBlob->putBlock(blockData);
	switch (result) {
	case DataBlob::errs::SUCCESS:
		bDesc.fileName = _wBlob->getFilename();
		bDesc.position =  writePosition;
		bDesc.hash = dataHash;
		break;
	case DataBlob::errs::ERR_BLOB_IS_FULL:
		// блоб заполнен, создаем новый
		_wBlobCount++;
		// заполняем имя файла
		blobPath = string(STORAGE_DIRECTORY) + "/" + string(STORAGE_FILENAME) + "_" + to_string(_wBlobCount);
		// пытаемся открыть
		if (_wBlob->open(blobPath) != DataBlob::errs::SUCCESS) {
			// произошло что-то странное, блоб не создан
			_wBlobCount--;
			goto exit;
		} else {
			// записываем данные
			writePosition = _wBlob->getPosition();
			result = _wBlob->putBlock(blockData);
			bDesc.fileName = _wBlob->getFilename();
			bDesc.position =  writePosition;
			bDesc.hash = dataHash;
		}
		break;
	default:
		goto exit;
	}

	// обновляем таблицы
	if (result == DataBlob::errs::SUCCESS) {
		_tableIndex.insert(pair<uint64_t, blockDesc>(blockId, bDesc));
		_tableHash.insert(pair<size_t, uint64_t>(dataHash, blockId));
	}

exit:
	return result;
}

int DataServer::getBlock(uint64_t blockId, char* blockData)
{
	int result = 0;

	// проверяем присутствие blockId в таблице
	map<uint64_t, blockDesc>::iterator findRes = _tableIndex.find(blockId);
	if (findRes != _tableIndex.end()) {
		// нашли, открываем блоб
		if (!_rBlob->open(findRes->second.fileName)) {
			// читаем данные
			result = _rBlob->getBlock(findRes->second.position, blockData);
		} else {
			// по какой-то причине блоб не открыт
			result = DataBlob::errs::ERR_BLOB_NOT_OPENED;
		}
	} else {
		result = errs::ERR_BLOCK_ID_NOT_EXIST;
	}

	return result;
}




